//
//  Model.swift
//  FamilyDoctorTest
//
//  Created by Artyom on 13.03.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import RealmSwift


class PillResponse: Codable {
    var status : String
    dynamic var pills: [Pill]
}

//struct PillResponse: Codable {
//    var status: String
//    var pills: [Pill]
//}

class Pill: Object, Codable {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var img = ""
    @objc dynamic var desription = ""
    @objc dynamic var dose = ""
    @objc dynamic var fullDescription: String{
        return desription + "\n" + dose
    }
    
    override static func primaryKey() -> String? {
           return "id"
       }
}



