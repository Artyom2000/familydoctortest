//
//  NetworkManager.swift
//  FamilyDoctorTest
//
//  Created by Artyom on 13.03.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    static let shared = NetworkManager()
    
    func getRequest(completion: @escaping(_ pillResponce: PillResponse?, _ error: Error?)-> ()){
        
        let url = "https://cloud.fdoctor.ru/test_task/"
        AF.request(url).validate(statusCode: 200..<300).responseData(completionHandler: { (response) in
            switch(response.result){
            case .success(_):
                let decoder = JSONDecoder()
                let data = response.data!
                
                do{
                    let pillResponse = try decoder.decode(PillResponse.self, from: self.normalizeJSON(data: data)!)
                    completion(pillResponse, nil)
                } catch let err {
                    print(err)
                    completion(nil, err)
                }
               
                break
            case .failure(_):
              
                completion(nil, response.error)
            }
         }
        )
  
    }
    
    func normalizeJSON(data: Data) -> Data? {
        
        guard let stringRepresentation = String(data: data, encoding: .utf8) else { return nil }
       let fixQuotesString = stringRepresentation.replacingOccurrences(of: #"\s(\w+):{1}"#, with: #""$1":"#, options: .regularExpression)
        
        let fixStatusValueString = fixQuotesString.replacingOccurrences(of: "'", with: "\"")
        
        let fixCommaString = fixStatusValueString.replacingOccurrences(of: #"[^,{ ](?=\n *["])"#, with: "\",", options: .regularExpression)
        return fixCommaString.data(using: .utf8)
    }
}
