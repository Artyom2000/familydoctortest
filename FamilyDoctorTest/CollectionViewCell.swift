//
//  CollectionViewCell.swift
//  FamilyDoctorTest
//
//  Created by Artyom on 13.03.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}
