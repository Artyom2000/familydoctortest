//
//  CollectionView.swift
//  FamilyDoctorTest
//
//  Created by Artyom on 12.03.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift
protocol CollectionManagerProtocol: class {
    func didEndDecelerating(_ scrollView: UIScrollView)
}


class CollectionManager: NSObject, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var pills = [Pill]()
    
    weak var delegate: CollectionManagerProtocol!
    
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return pills.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        let url = URL(string: pills[indexPath.row].img)
        cell.imgView.sd_setImage(with: url, placeholderImage: UIImage(imageLiteralResourceName: "GrayPlaceHolder"), completed: nil)
        return cell
    }
    
    

    
       func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate.didEndDecelerating(scrollView)
  
    }
    
    
    
   

}
