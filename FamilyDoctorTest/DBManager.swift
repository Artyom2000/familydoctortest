//
//  DBManager.swift
//  FamilyDoctorTest
//
//  Created by Artyom on 15.03.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import UIKit
import RealmSwift
class DBManager {
    private var database: Realm
    
    static let sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func getDataFromDB() ->   [Pill] {
      let results: Results<Pill> =   database.objects(Pill.self)
        return [Pill](results)
     }
    
    func saveData(objects: [Pill])   {
            try! database.write {
                for obj in objects{
                    database.add(obj, update: .modified)

                }
               
            }
       }
    
    
    func deleteAllFromDatabase()  {
         try!   database.write {
             database.deleteAll()
         }
    }
}
