//
//  MainViewController.swift
//  
//
//  Created by Artyom on 12.03.2020.
//

import UIKit
import Alamofire
import UPCarouselFlowLayout
import RealmSwift
class MainViewController: UIViewController, CollectionManagerProtocol {
    
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var pageControll: UIPageControl!
    
    private let collectionManager = CollectionManager()
    
    
    fileprivate var pageSize: CGSize {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        pageSize.width += layout.minimumLineSpacing
        
        return pageSize
    }
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            let pill = self.collectionManager.pills[self.currentPage]
            animateLabels()
            self.nameLabel.text = pill.name
            self.descriptionLabel.text = pill.fullDescription
            self.pageControll.currentPage = currentPage
        }
      }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = collectionManager
        collectionView.delegate = collectionManager
        collectionManager.delegate = self
        getDataFromDB()
        getRequestPills(currentPage: pageControll.currentPage)
        
    }
    
    
    func getDataFromDB() {
        collectionManager.pills = DBManager.sharedInstance.getDataFromDB()
        if collectionManager.pills.count != 0 {
            self.pageControll.numberOfPages = collectionManager.pills.count
            self.currentPage = self.pageControll.currentPage
        }
    }
    
    func getRequestPills(currentPage: Int) {
        NetworkManager.shared.getRequest { (pillResponse, error) in
               if error == nil {
                       
                   
                   self.collectionManager.pills = pillResponse!.pills
                DBManager.sharedInstance.saveData(objects: pillResponse!.pills)
                   self.collectionView.reloadData()
                   self.pageControll.numberOfPages = pillResponse!.pills.count
                self.currentPage = self.pageControll.currentPage
                   
               }else{
                self.alert(message: error?.localizedDescription ?? "Network problem", title: "Error")
            }
           }
    }
    
    override func viewWillLayoutSubviews() {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    @IBAction func didTapContinue(_ sender: UIButton) {
        if currentPage + 1 >= collectionManager.pills.count { return }
        currentPage = currentPage + 1
        collectionView.scrollToItem(at: IndexPath(row: currentPage, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    
    func didEndDecelerating(_ scrollView: UIScrollView) {
        let pageSide =  self.pageSize.width
        let offset = scrollView.contentOffset.x
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
    
    @IBAction func didTapRefresh(_ sender: Any) {
        getRequestPills(currentPage: pageControll.currentPage)
    }
    
    func animateLabels() {
        
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.subtype = CATransitionSubtype.fromTop
        
        animation.duration = 0.3
        
        self.nameLabel.layer.add(animation, forKey: CATransitionType.fade.rawValue)
        self.descriptionLabel.layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
}




